import { Component } from '@angular/core';
import { Router } from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private currentUser: any;

  constructor(private router: Router, private http: HttpClient) {
  }

  goToMyProfile() {
    this.currentUser = JSON.parse(this.getUser());
    this.router.navigateByUrl('/profile/' + this.currentUser.id);
  }

  goToMySuggestions() {
    const user = JSON.parse(this.getUser());
    this.router.navigateByUrl('/suggestions/' + user.id);
  }

  logout() {
    localStorage.removeItem('user');
    this.router.navigateByUrl('/');
  }

  getUser() {
    return localStorage.getItem('user');
  }
}
