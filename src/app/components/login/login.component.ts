import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  error: string;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.form = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required])
    });
    const user = this.getUser();
    this.router.navigateByUrl('/suggestions/' + user.id);
  }

  get username() { return this.form.get('username'); }
  get password() { return this.form.get('password'); }

  submit() {
    this.http.post('http://localhost:8080/login', this.form.value).subscribe(
      response => {
        if (response) {
          this.error = null;
          localStorage.setItem('user', JSON.stringify(response));
          const user = this.getUser();
          this.router.navigateByUrl('/suggestions/' + user.id);
        } else {
          this.error = 'Invalid credentials...';
        }
      }, error => {
        this.error = error.message;
      }
    );
  }

  getUser() {
    const userString = localStorage.getItem('user');
    if (userString) {
      return  JSON.parse(userString);
    }
  }

}
