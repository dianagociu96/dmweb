import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../../models/user';
import { StudyLevel } from '../../../models/StudyLevel';
import { Gender } from '../../../models/Gender';
import { Work } from '../../../models/Work';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  private userId: any;
  private user: any;
  gender = Gender;
  studyLevel = StudyLevel;
  work = Work;
  constructor(private route: ActivatedRoute, private http: HttpClient) { }
  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.userId = params['id'];
      this.getUserProfile(this.userId);
    });
  }

  getUserProfile(id) {
    this.http.get('http://localhost:8080/find/' + id).subscribe(result => {
      this.user = result;
    });
  }
}
