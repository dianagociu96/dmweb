import {Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';

import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {User} from '../../../models/user';
import {HttpClient} from '@angular/common/http';
import {Gender} from '../../../models/Gender';
import {StudyLevel} from '../../../models/StudyLevel';
import {Work} from '../../../models/Work';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  user: any = {};
  gender = Gender;
  studyLevel = StudyLevel;
  work = Work;
  hobbies = [
    {name : 'SPORTS', id: 1},
    {name : 'TV_SERIES', id: 2},
    {name : 'GAMING', id: 3},
    {name : 'ARTS_AND_CRAFTS', id: 4},
    {name : 'READING', id: 5},
    {name : 'GARDENING', id: 6},
    {name : 'COOKING', id: 7},
  ];
  private hobbiesString = '';
  error: string;

  constructor(private formBuilder: FormBuilder, private http: HttpClient, private router: Router) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      age: ['', Validators.required],
      height: ['', Validators.required],
      gender: ['', Validators.required],
      preferedGender: ['', Validators.required],
      fieldOfWork: ['', Validators.required],
      studyLevel: ['', Validators.required],
    //  hobbies: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }
  onChange(hobby: string, isChecked: boolean) {
    if (isChecked) {
      this.hobbiesString += hobby;
      this.hobbiesString += ',';
    }
  }
  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    this.hobbiesString = this.hobbiesString.slice(0, -1);

    this.user.userHobbies = this.hobbiesString;
    if (this.registerForm.invalid) {
    } else {
      this.http.post('http://localhost:8080/register', this.user).subscribe(
        response => {
          if (response) {
            this.error = null;
            this.router.navigateByUrl('/login');
          } else {
            this.error = 'Acest user exista deja!';
          }
        }, error => {
          this.error = error.message;
        }
      );
    }
  }

}
