import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Gender } from '../../../models/Gender';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-suggestions',
  templateUrl: './suggestions.component.html',
  styleUrls: ['./suggestions.component.css']
})
export class SuggestionsComponent implements OnInit {
  people: any;
  gender = Gender;
  userId: number;

  constructor(private http: HttpClient, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.userId = params['id'];
    });
    this.http.get('http://localhost:8080/suggestedPersons/' + this.userId).subscribe(
      response => {
        this.people = response;
        console.log(this.people);
      }, error => {
        console.log(error);
      }
    );
  }

}
