import {Gender} from './Gender';
import {StudyLevel} from './StudyLevel';

export class User {
  id: number;
  username: string;
  gender: Gender;
  height: number;
  age: number;
  preferedGender: Gender;
  studyLevel: StudyLevel;
  fieldOfWork: string;
  userHobbies: string;
  password: string;
  firstname: string;
  lastname: string;
}
